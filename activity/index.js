console.log("Hello World");
/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
function printWelcomeMessage(){
	let firstName = prompt("Enter your first name: ")
	let lastName = prompt("Enter your last name: ")
	let age = prompt("How old are you? ")
	let cityAddress = prompt("What city do you live in? ")

	console.log("Hello, " + firstName + " " + lastName);
	console.log("You are " + age + " " + "years old.")
	console.log("You live in " + cityAddress)
}

printWelcomeMessage();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
function favoriteMusicalArtists(){
	let artist1 = "EXO";
	let artist2 = "BTS";
	let artist3 = "IU";
	let artist4 = "BlackPink"
	let artist5 = "LeeHi"

	console.log("1. " + artist1)
	console.log("2. " + artist2)
	console.log("3. " + artist3)
	console.log("4. " + artist4)
	console.log("5. " + artist5)
};

favoriteMusicalArtists();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
function favoriteMovies(){
	let movie1 = "Avatar: The Way of Water";
	let movie2 = "Inside Out";
	let movie3 = "John Wick: Chapter 4";
	let movie4 = "Toy Story 2";
	let movie5 = "Coco";

	console.log("1. " + movie1);
	console.log("Rotten Tomatoes Rating: 92%");
	console.log("2. " + movie2);
	console.log("Rotten Tomatoes Rating: 98%");
	console.log("3. " + movie3);
	console.log("Rotten Tomatoes Rating: 94%");
	console.log("4. " + movie4);
	console.log("Rotten Tomatoes Rating: 100%");
	console.log("5. " + movie5);
	console.log("Rotten Tomatoes Rating: 97%");
};

favoriteMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


function printUsers(){
	alert("Hi! Please add the names of your friends.");

	function printFriends(){
			let friend1 = prompt("Enter your first friend's full name: "); 
			let friend2 = prompt("Enter your second friend's full name: "); 
			let friend3 = prompt("Enter your third friend's full name: ");

			console.log("You are friends with:");
			console.log(friend1); 
			console.log(friend2); 
			console.log(friend3); 
		};
		printFriends();
	};


printUsers();