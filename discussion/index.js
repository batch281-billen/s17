// Function
/*
	Syntax:
		function functionName(){
			code block (statement)
		};
*/
function printName(){
	console.log("My name is Viel");
};

printName();

// Function declaration vs expressions

// Function declaration
function declaredFunction(){
	console.log("Hello, world from declaredFunction()");
};

declaredFunction();

// Function expression - declaring a variable then the initial value of that variable is a function. A function without a name is an anonymous function (e.g. function())
let variableFunction = function(){
	console.log("Hello again!");
};

variableFunction();

declaredFunction = function(){
	console.log("Updated declaredFunction");
};

declaredFunction();

const constantFunc = function(){
	console.log("Initialized with const!");
};

constantFunc();
/*
constantFunc = function(){
	console.log("Cannot be re-assigned");
};

constantFunc();
*/

// Function scoping - eto lang yung pwde ma-access sa isang function

/*
	Scope is the accessibility (visibility) of variables

	JS Variables have 3 types of scope:
		1. local/block scope
		2. global scope
		3. function scope
*/

{
	let localVar = "Alonzo Mattheo";

	console.log(localVar);
}

let globalVar = "Aizaac Ellis";

console.log(globalVar);

function showNames() {
	// Function scoped variables
	var functionVar = "Joe";
	const functionConst = "John";
	let functionLet = "Jane";

	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);

}

showNames();

// console.log(functionVar);
// console.log(functionConst);
// console.log(functionLet);

// Nested Functions

function myNewFunction(){
	let name = "Jane";

	function nestedFunction(){
		let nestedName = "John";
		console.log(name);
	}

	nestedFunction();
}

// nestedFunction();
myNewFunction();

// Function and Global scoped variables

// Global scoped variable

let globalName = "Joy";

function myNewFunction2(){
	let nameInside = "Kenzo";

	console.log(globalName);
	console.log(nameInside);
}

myNewFunction2();
// nameInside(); - hindi lalabas si Kenzo kasi tinawag sya sa labas ng myNewFunction2 function code block.
// console.log(nameInside);
// Using alert() - this will run immediately when the page loads.

// Yung "Hello, world!" muna lalabas then after clicking ok lalabas naman si "Hello, user!" kasi tinawag si showSampleAlert.
alert("Hello, world!"); 

function showSampleAlert(){
	alert("Hello, user!")
}

showSampleAlert();

// Using prompt() - kung ano yung inenter na name sa prompt, yun yung lalabas na value ng samplePrompt.

let samplePrompt = prompt("Enter your name: ");

console.log("Hello, " + samplePrompt);

function printWelcomeMessage(){
	let firstName = prompt("Enter your first name: ");
	let lastName = prompt("Enter your last name: ");

	console.log("Hello, " + firstName + " " + lastName + "!");
	console.log("Welcome to my page!");
}

printWelcomeMessage();

/*
Function naming conventions
	- Function names should be definitive of the task it will perform.
	- Avoid generic names to avoid confusion within the code (e.g. get).
	- Avoid pointless and inappropriate function names.
	- Name your functions following camel casing.
	- Do not use JS reserved keywords.
*/